# Dolphin Scheduler Noun

### Dolphin Scheduler Noun
| Standard | Specification |
| :---: | :---: |
| RESTful | Representational State Transfer |
| Spring Boot |  |
| Spring Cloud |  |
| Web |  |
| Java |  |
| JVM | Java Virtual Machine |
| Golang |  |
| Python |  |
| Linux |  |
| DB | DataBase |
| MySQL |  |
| JSON | JavaScript Object Notation |
| IP |  |
| Hadoop |  |
| HDFS | Hadoop Distributed File System |
| MapReduce |  |
| HBase |  |
| Yarn |  |
| Kafka |  |
| ZooKeeper |  |
| Elasticsearch |  |
| Logstash |  |
| Kibana |  |
| CPU | Central Processing Unit |
| PostgreSQL |  |
| Kerberos |  |
| NFS | Network File System |
| HTTP | Hyper Text Transfer Protocol |
| SkyWalking |  |
| gRPC | google Remote Procedure Call |
| SMTP | Simple Mail Transfer Protocol |
| TLS | Transport Layer Security |
| DolphinScheduler |  |
