hello大家好，
hello,everyone
今天我们来告诉大家如何快速的上手安装ds
today, im going to demostrate how to quick start DS , based on docker deployment
首先我们google关键字ds进入他的官网,然后我们点击 DOCS，进入最新的1.3.6版本,点击部署文档里的docker deployment
Above all, we google the keyword DS and access the offical website,after that click the DOCS navigation bar and select version 1.3.6,
select Docker Deployment under the Deployment Document  /ˈdɒkjumənt/ 
首先我们用到了DOCKER和DOCKER COMPOSE这两个工具
From the document   /ˈdɒkjumənt/ , we can see that, we use 2 tools :docker and DOCKER COMPOSE 
我们点击docker链接进入了docker的官网来检查版本,我们需要的是1.13.1以上的版本,1.13版本是一个很早前的版本
we click the docker hyperlink and access docker offical website, to confirm the version. what we need is version above 1.13.1,and that is a  very early version.
这个docker我已经安装好了，我们可以看一下我们虚拟机里的版本是20.10.1版本，非常新的版本
I have done the docker installation, we could see the docker version of the virtual machine is 20.10.1, a  very new version.
docker-compose我们同样需要1.11.0以上的版本
Furthermore  /ˌfɜːðəˈmɔː(r)/ , we also need a version above 1.11.0 for docker-compose
点击进入他的官网，docker-compose它的官网的介绍是这样的：
他是一个工具用来定义和运行多个docker 应用容器,当你用到docker compose，你可以直接用yaml文件来配置你的应用服务,这样可以实现一个指令根据配置创建许多服务,这样就可以省去很多的工作。
Click into its offical site, it describe compose as a tool to define and operate multi docker application containers. With the capability of compose, you are capable to use yaml to configure your application services. which enables one command to create multi services, saves a lot of work .
我们需要确定docker compose的版本,compose需要python3和pip3的,我都已经安装过了，compose是1.27版本，也是满足前置要求
we need to confirm the version of compose, compose dependent on python3 and pip3, which I have already done installation,and our compose version is 1.27, satisfy prerequisite./priːˈrɛkwɪzɪt/.

根据文档描述，首先要排除一些物理硬件的要求
According to the document, we need to exclude some hardware requirements
现在进行第一步，去下载源码包，可以直接点击链接进行下载，目前我是已经下好了tar包，放在tmp
根据文档第二步把这个tar包解压
And Then we move on to the first step, go download the source code package. just click the hyperlink to download. Currently i have done the tar package download and put it under tmp directory.
We decompress the tar package following the 2nd step 
目前我是已经解压好了，放在/usr/local目录下
At present, i have done the decompression under the /usr/local directory
apache-dolphinescheduler,安装在这个目录底下
apache  /əˈpætʃi/-dolphinescheduler,under this directory
下一步需要cd到/docker/docker-swarm文件夹下,目前我们已经在指定的目录下
Next we need to change directory to the /docker/docker-swarm
and currently , we are under the specified  /ˈspesɪfaɪd/ directory
下一步我们需要安装docker  ds 1.3.6版本的镜像,复制指令安装一下,在安装之前可以search一下ds目前最新的版本是什么版本
Next we are going to install DS version 1.3.6 image from the official docker repository, copy the install command. And we could search DS latest version before installation 
我们等待ds的安装
We now waiting the installation
好了，目前这个镜像的安装就ok了
alright, the image installation is done
下一步根据文档，将1.3.6版本打上latest标签,ok latest标签打上了
Next according to the document, tag version 1.3.6 as the latest.
ok the tag is done
下一步我们必须在docker docker-swarm文件夹下执行 docker-compose up的命令
The following command: docker-compose up. must be run under docker docker-swarm directory.
让他帮助我们进行安装和启动,首先他会去下载ds以外的引用,比如说zookeeper和postgresql的镜像依赖,创建它的volume
Let compose help us to install and run. Firstly, it will download references other than DS, such as zookeeper and postgresql (/ˈpoʊstɡrɛs ˌkjuː ˈɛl/) images, and then create its volume  /ˈvɒljuːm/ 
然后会启动zk和数据库以及4个服务,目前的话都是done说明启动已经ok了
After that it will start zookeeper, database and other 4 DS main services.
Currently, all done status shows they are started  /'stɑːtɪd/.
然后我们可以检查一下,目前的话，zk和postgresql已经启动了
Then we can check the docker container status, 
Currently, zookeeper and postgresql is done
然后我们4个主要的服务,健康状态是starting状态,我们需要等待服务的启动完成，因为他是需要时间的
And about our other 4 services, the healthy condition is starting, we need to wait em to finish because they need time to start.
ok还有一个服务是starting,等他全部启动完成之后,我们就可以进入他的管理界面,这个时候所有的状态都变成是healthy
ok, still one left starting condition,after all services done, we could enter its management webview, and at this time, all status turn to be healthy.



sp我们进入管理界面
we open the management page
使用初始账号admin
use initial account admin and password dolphinscheduler123
初始密码是123
这样我们成功打开了ds的管理界面
In this way, we successfully opened the management interface of DS
我们可以大概的进行浏览
we can roughly browse the interfaces
今天的quick-start视频就是这样
And our quick-start video ends here
Thanks!
谢谢大家
  