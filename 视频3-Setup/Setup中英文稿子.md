本地开发环境搭建教程

今天来告诉大家如何在本地搭建DS源码的开发环境以及对DS源码的本地启动和调试
hi everyone, Today, we are going to know how to Set-up DolphinScheduler Development Environment and Run Source Code on our Windows OS locally.
首先我们打开DS官网，找到开发者菜单后，进入开发者指南。他非常详细的介绍了如何搭建开发环境，以及如何对前端后端进行开发。
To begin with, visit DS official website and check the DEVELOPMENT navigation.Then we could see the Developer guide, which clearly introduces how to build DS development environment and the preparations before frontend and backend development. 
那么我们跟随官方的指引，进行DS的搭建，启动与调试。这里的笔记，我提前汇总了整个搭建过程
Then we are going to build, start and debug DS by the official guidance. 
Here we have a note, for this note,I summarized the entire process in advance.


首先，去github上下载所有的源码，我们打开DS的github。
在github上我们可以通过git clone或者直接download code。
Above all, go to github to download the whole DS project.
On github, we can use git clone command or download the code package directly.
我们这里选择浏览器下载zip压缩包，这里已经下好了。
Here we choose to download the zip package in the browser which has been already downloaded here. 
接下来我们需要在本地安装一个zk。
首先我们打开zk官网，进入releases页面选择一个版本进行下载。这里也已经下载好了。
And next, we need to intall zookeeper on our Windows.
First visit zookeeper official website and open the releases page, select a version and download it. We also have already downloaded here.

首先先解压zk和ds的压缩包，
我们先进行zk的安装。打开文件夹。需要在里面新建两个目录，data和log。data用于存放zk的数据信息，log用于存放zk的日志信息。
 In the first place, we unzip both zk and ds package.
By sequence, we deal with zookeeper installation first. To begin with, We need to create data and log two folders under zookeeper directory, the data folder is used to store zk data and log one is used to store zk logs information.

然后我们进入conf配置文件夹，将zoo_sample.cfg复制一份，并更名为zoo.cfg.
然后修改里面的参数，将data和log路径设置到配置文件中。dataDir和dataLogDir。
After that we go to the zookeeper conf folder, Copy the zoo_sample.cfg file in the conf directory, rename it to zoo.cfg and modify the data and log directory settings。

下来进入bin目录。直接点击zkServer.cmd文件进行启动。当我们看到命令行窗口就是启动了。
Afterwards, we go to the bin folder and run zkServer.cmd to start zk service. The service starts when we see the command windows.
我们再打开客户端zkcli进行连接验证，
输入ls /查看根节点的状态。看到一个节点叫zookeeper，说明zk的启动时成功的。
这样安装和启动zk就完成了。
Then we run zkCli.cmd to use client to verify whether zk service run smoothly.
run ls(list file) / to check the root directory and we could see a node called zookeeper, which means zk service run successfully.
For this,the installation and startup of zk is complete.

下来我们为ds新建一个调试用的数据库。
After zookeeper is done, we are going to create a debug database for DS.
我们通过mysql连接工具navicat，创建一个数据库dstest。字符集选择utf8。
We create a new database dolphinschedulerTest by using navicat, a mysql connector tool and select utf8 character set.

下来我们将源码导入到IDE中。在IDEA里选择open解压的ds文件夹。
Then we import souce code into ide(Integrated Development Environment ) by the Open function of  IntelliJ IDEA, 
接下来idea会根据maven依赖进行下载和解析。
By following, Idea will download repositories and resolve dependencies according to maven.
待IDE读取完毕。

然后我们需要做一些配置修改
我们需要把pom中，mysql-connector-java的scope改为compile。
打开pom文件搜索connector，进行修改。
重新reimport maven。
Then we need to do some configuration modifications.
We need to change the scope of mysql-connector-java to compile.
Open the pom.xml and search keyword mysql connector to modify the scope. And click reimport maven to refresh maven.

然后将dao模块中默认的mysql datasource设置改为我们刚才创建的库信息。
我们打开dao模块，resource里的datasource.properties文件。将mysql配置修改并注释postgresql配置。
接下来运行dao模块初始化数据库方法，这个方法会将ds所需要的数据和表自动导入目标数据库。
我们直接运行CreateDolphinScheduler里的main方法。代码就开始向数据库导入数据。
结束之后，我们可以刷新数据库，所有的初始数据已经创建完成。
Next, we need to change the mysql datasource configuration of dao module to the new created database.
We open dao module, find the datasource.properties inside resources folder. annotate  /ˈænəteɪt/  postgresql config and add mysql config.
After the datasource is done. we run the database init method of dao module, this method will create all the tables and data and import em to target database which ds needs.
We just run the main method of CreateDolphinScheduler, the code will do the rest job to import data.
After finished, refresh database and we could see all the data and tables are complete.

下来我们需要，将ds-service模块的zk配置改成我们本地启动的zk配置。localhost:2181.
打开service模块，resources文件夹里的zookeeper.properties文件。如果已经是localhost:2181就不需要修改了。
下来我们可以将worker server日志添加在控制台中，打开server模块，resources文件夹下，logback-worker文件中，在logback配置中添加STOUT本地控制台的打印。
同样可以在master、api模块中添加stout日志打印。
Next we need to change zk configuration of ds-service module  to our local zk. localhost and port 2181.
Open the service module and find zookeeper.properties inside resouces folder. we could see there is already localhost and 2181 so we dont need to change.

Then we are going to add standard worker server log output to the console. Open server module and find logback worker.xml in the resources folder, add STandard OUTput in the logback configuration to print worker server logs 
And the same, We could also add standard log output in master and api modules.

下来我们启动4个主服务。
第一个是执行 org.apache.dolphinscheduler.server.master.MasterServer 的 main 方法 
搜索MasterServer，在server模块下。
在启动之前，需要添加一些启动参数配置。在IDEA中新建一个application，选择masterserver这个类。设置vm options启动参数，指定日志配置文件和druid参数。将模块的classpath设置为server模块
After log setting is done, start 4 main services.
The first one is main method of MasterServer

Before we start master service, we need to configure some vm options. Create a new application in IDEA and select masterserver class. set vm options for logging and druid  /ˈdruːɪd/ . And set servermodule as classpath. 

然后再找到workerserver，做同样的参数配置。设置为worker日志配置文件以及server模块classpath。这样master和 worker就配置好了
然后还需要配置apiserver，同样指定日志文件以及api模块。
这样3个启动模块就配置完成。
Then we go to find worker server and do the same settings to worker. Add worker logback file and druid to vm options. and set classpath.
There is one apiserver left,assign logback file and active profile to apiserver.
In this way, the startup of 3 main modules are configured.

下来我们尝试启动，先启动master，再启动worker，再启动api
如果我们要用到日志功能，那么可以直接启动loggerserver，不需要任何配置。日志功能包含，工作流实例执行后的查看任务日志功能。

这个时候可以尝试访问后端的swagger文档。后台的接口都已经可以列出来了。说明启动成功。
Then we try to start main services, first master ,and then worker , api at last.
If we want to use the log function, we can start the loggerserver directly without any configuration. The log function enables us to check the task log after the execution of the process instance.
At this time, we can try to access swagger document.And if APIs are listed on the website, it shows that the startup is successful.


然后我们着手启动前端。打开ui模块。打开windows explorer
我的node版本是12.20.2
首先npm install 安装前台需要的相关依赖,nodejs最好是
安装完成后，我们通过npm run start启动他
然后访问localhost: quadruple  /kwɑːˈdruːpl/ 8888, 使用默认的账号admin 密码是ds123.登录成功
After the backend is deployed, we proceed /prəˈsiːd/  to start the front-end. Go into the UI module and open it in windows explorer./ɪkˈsplɔːrə(r)/
The node version is 12.20.2
Run npm install to install relevant dependencies that front-end requires. Ihave already downloaded beforehand.
And after that, run 'npm run start' to start the front-end.
Visit localhost and port 8888 quadruple/kwɑːˈdruːpl/ 8, and sign in with default account, username admin and password is dolphinscheduler123.
可以看到我们已经成功的启动了ds前台，后台。这样我们就可以在本地环境进行DS开发和调试！
Finally, we have successfully started ds frontend and backend. Now we could move forward to develop and debug DS on our local environment!
Thats somuch for todays video, thanks!