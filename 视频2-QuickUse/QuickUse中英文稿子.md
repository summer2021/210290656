
video-3稿子
这一期将告诉大家如何简单使用 DolphinScheduler ,并发布 SHELL 任务
Hello everyone, this episode /ˈepɪsəʊd/ gonna introduce how to simply use DS and publish shell task
5 个服务
首先我们需要检查我们的 DolphinScheduler 服务是否正常运行，DolphinScheduler 包含以下 5 个服务
First, we need to check whether our DS services is running regularly and DS contains the following 5 main services:
1. ApiApplicationServer: 负责是为用户UI界面提供接口调用的服务
is service Responsible for providing interfaces calling for  UI APIs 
2. MasterServer: 负责整个工作流调度 
is service Responsible for whole workflow scheduling service
3. WorkerServer: 具体执行工作流任务的 worker 服务
is service Responsible for specified workflow handling 
4. LoggerServer: 记录任务运行状态的日志服务
is service Responsible for  record rɪˈkɔːd/  task running status
5. AlertServer: 负责对工作流的异常、失败情况进行告警
is service Responsible for  alerting abnormal and failure workflows
我们执行命令查看进程是否都在运行
and we execute the comand to check whether processes sis are running
（拓展 linux 查看进程命令 ps -ef | grep dolphin.....）
登录页登录
确保服务没有问题后，我们打开登录页进行登录，路径是(单机部署：安装) |（集群部署：api服务）所在服务器的ip:12345/dolphinsheduler 使用管理员的默认账号密码 admin dolphine scheduler1234。进入后，我们来到了安全中心页面。
After ensuring services are normal, we are  going to visit the login page, the url is ip with port 1234 and the uri path is /Slash dolphinsheduler. we sign in with the default admin account, the username is admin and the password is dolphine scheduler1234. After sign in,we come to the security page.
首先，进行初步的用户与任务的配置。
Above all, we are going to configure  user and workflow basic info 
我们的整体流程是 创建租户-查看队列-创建用户-配置新的告警组管理-了解worker分组管理 
Our overall  /ˌəʊvərˈɔːl processes are, create tenant- check queue- create user- configure new warning group- view worker group
and then shift to new user to execute workflow
再切换新用户去执行工作流

租户管理-创建租户 
菜单的第一个是租户
租户对应的是Linux用户，是用于worker提交作业所使用的用户。如果linux上没有这个用户，worker会在执行脚本的时候会创建这个用户
我们创建一个新的租户tenant_002 队列使用默认的队列
The first menu tab is tenant, The tenant corresponds to the Linux user, which is the user used by the worker to submit job. If the user inexist on linux, the worker will create this user when executing scripts,
We create a new tenant named tenant underscore "double-O-two" 002 and use the default queue.
队列管理-创建队列 (如果本期讲解不涉及到队列相关，可以直0.2T接跳过 不做讲解)
队列只针对跑在yarn上的任务生效，这里可以进行创建，因为我们是shell任务与yarn无关就不进行创建了，使用默认队列
queue is only valid for tasks running on yarn. You can create it here, but We will not create them because shell tasks that we gonna implement have nothing to do with yarn. just Use the default queue.
#这个定义的是yarn上的application queues，只针对跑在yarn上的任务生效。然后我们尝试创建一个新的队列（备注：我们先暂时不做深入理解 yarn 是什么）
用户管理-创建用户
然后我们点击用户管理，创建新的用户，填写登录信息，并将新建的租户和默认队列绑定给这个用户
Then we click User Management, create a new user, fill in the login information, and bind the new tenant and default queue to this user
告警组管理
创建新的告警组，选择邮件通知的方式，并将新用户添加至告警组
then we Create a new alarm group named alarm_underscore "double-O-one" 001, select the email notification, and add the new user to the alarm group
worker分组管理
1.3.0之后从zk中直接读取worker配置信息
After version 1.3.0,DS read worker configuration information directly from zookeeper
这样初步的创建就完成了
Then the initial configuration is complete
默认是不使用admin账户运行工作流任务的，因为我们可以看到admin默认没有配置租户和队列，如果一定要使用admin，那需要先给admin指定租户和队列。
under normal situation will not use the admin account to run workflow tasks, because we can see that admin doesn't configure tenants and queues by default. If you must use admin, you need to specify tenants and queues for admin first.
做好新用户的配置后，我们切换新创建的用户
After completed the  new user configuration, we switch to the new account user_underscore  "double-O-one" 001

然后我们将实践以下步骤
Then we will practice the following steps
工作流定义-运行工作流-工作流实例管理-超时任务-重跑 \暂停-创建资源文件
Create Process Definition-Run Process-Manage Process Instance-Timeout-Rerun/Pause-Create Resource File
创建项目-工作流定义-shell任务
首先我们进入项目管理，创建新的项目，然后点击进入项目，选择工作流定义，给项目创建新的工作流。
First we enter the project management, create a new project, then click into the project, select  process definition, and create a new process for the project.
我们就进入了DAG的编辑页面，我们可以通过拖拽的方式新增各种类型的任务，包含SHELL,SUB_PROCESS,PROCEDURE,SQL,SPARK,FLINK,MAPREDUCE,PYTHON,DEPENDENT,HTTP,DATAX,SQOOP,CONDITIONS.
then We enter the edit page of DAG, we can add various types of tasks by dragging and dropping, such as
SHELL,SUB_PROCESS,PROCEDURE,SQL,SPARK,FLINK,MAPREDUCE,PYTHON,DEPENDENT,HTTP,DATAX,SQOOP,CONDITIONS.
我们以简单的shell任务举例，拖拽shell任务节点到画布中，就可以编辑我们的任务设置，首先填写节点名称，运行标志表示我们在运行整个工作流时，这个任务节点是否执行。描述就不填了。任务优先级表示，在线程数不足时，任务运行的优先级将会按选择的优先级进行排列处理。
worker选择默认的。如果失败重试次数为1并且失败间隔为1分钟，则代表任务如果执行失败，在一分钟之后会重试运行一次任务。
超时告警里包括超时策略和超时失败。超时策略表示，按照超时时长的设置，如果超时，就会抛出告警。超时失败表示如果超时工作流结果便为失败。
Let's take a simple shell task as an example. Drag and drop the shell task node to the canvas to edit our task settings. 
First, fill in the node name. The run flag indicates whether this task node is executed when we run the entire process. 
Just omitt the  description. 
Task priority means that when the  thread resource is insufficient, the priority of task execution will be arranged and processed according to the selected priority. 
Then we choose the default worker . If we set one time failed retries and 1 minute failure interval , it means that if the task fails, the task will be retried once one minute later.
The timeout alarm includes timeout alarm and timeout failure. The timeout alarm means that an alarm will be thrown out if the task expires timeout duration, . 
Timeout failure means that the process result would be a failure if the timeout expires.
脚本我们简单echo打印几句话 start running finished。
we simply echo print a few status in the shell script .shell task start, running and finished
资源表示引用资源中心的资源，因为暂时没有先不填。
Resources refer to the resource in the resource file management, we skip here for no resources configurd.
自定义参数，表示本次任务可设置的参数值。比如我们设置name参数，value为dolphin，则可以在脚本中用$/${}(1.3.6 测试需带上{})符号 进行引用
Custom parameters indicate the parameter values we can set for this task.
For example, if we set a parameter with key name and the value dolphin, we can use the parameter by  $ dollar and braces/${} (1.3.6 test with {}) symbol 
-工作流保存-运行工作流
我们这个工作流只设置一个shell任务节点，
we only sets up one shell task node in the whole process,
然后我们保存工作流，描述不填，租户选择我们新创建的租户，这个超时告警指的是整个工作流的，之前的设置是针对单个任务的。这里我们不设置超时。
Then we save the process, skip the description, and select the new tenant  "double-O-two"  002. This timeout alarm refers to the entire process. The previous alarm is for a single task node. Here we do not set a timeout.
这里还可以设置全局变量，这里我们添加一个全局变量gloabal_age，value为18，保存整个工作流。然后再进入shell任务，先在自定义参数中获取global_age，再在脚本中用$符号使用
we can set global variables here. Here we add a global parameter gloabal_(underscore)age with a value of 18 to save the entire process. Then enter into the shell task again, 
first get the global_age in the custom parameters, and then use the global parameter  by $ dollar and braces/${} symbol in the script
-生成工作流实例-查看工作流实例-全局\局部参数-工作流实例管理（重跑  暂停）
工作流创建好之后，我们准备运行工作流。首先要保证工作流是上线状态，然后点击运行。
失败策略描述的是针对并行任务，假设有两个并行任务A和B，A任务后有A1,A2,A3到AN很多子任务节点，B任务后也有很多子任务节点，如果A任务节点失败，选择继续则不影响B任务执行。如果选择结束，如果A任务失败之后，B任务也不再执行，整个工作流为失败状态。
After finish the process definition, we are ready to run the process. First, ensure that the workflow is online, and then click Run button.
The failure strategy is designed for parallel tasks. Suppose there are two parallel tasks A and B. After task A, there are many subtask nodes from A1, A2, A3 to AN, and there are many subtask nodes after task B too. 
If task A fails, Choosing to continue does not affect the execution of task B. If you choose to end, if task A fails, task B is no longer executed, and the entire workflow is in a failure state.
通知策略设置如何触发通知，选项中会根据不同结果设置是否发送通知。
Notification strategy sets how to trigger notifications. and these Options will set whether to send notifications according to different results.
流程优先级同样指线程不足时，会按照这个优先级设置去执行。
Process priority also refers to a condition when there are insufficient threads, it will be executed according to this priority sequence.
worker分组选择默认的zk worker分组
And we select default worker group.
通知组我们可以选择刚才创建的。
We could select alarm_001  "double-O-one"   as Notification group.
收件人，可以根据需求填写邮箱，抄送人同样可以设置抄送对象邮箱。
We can fill in the recipient  /rɪˈsɪpiənt/ and Carbon Copy as needed
补数：包括串行补数、并行补数2种模式。串行补数：指定时间范围内，从开始日期至结束日期依次执行补数，依次生成N条流程实例；并行补数：指定时间范围内，多天同时进行补数，同时生成N条流程实例。 
And about complement. Including two modes: serial complement and parallel complement. Serial complement means Within a specified time range, execute the complements from the start date to the end date,every each day and generate process instances ; parallel complement means within the selected time range, multiple days are complemented at the same time and generate process instances.
然后我们来运行一下。工作流运行之后，会产生一条 工作流实例，我点进去，在上方我们可以看到全局和自定义参数数值以及我们配置的启动参数。
Then we run it. After the process running, a process instance is generated. click it. On the top of the page, we can see the global and custom parameter values and the startup parameters we configured.
同时能看到运行时间，只是x秒。
At the same time, we can see the  time consuming, just seconds.
点击查看日志，这里都打印出来了，start，running，finish，以及我们的自定义参数和 全局参数都获得了。
Click to view the log, it is all printed here, start, running, finish, and our custom parameters and global parameters are all shown.
虽然我们设置了超时告警，但是并未超时，所以没有触发告警。
Although we set a timeout alarm, the process did not time out, so no alarm was triggered.
这里我们添加sleep代码让脚本超时，然后保存一下。这里有勾选是否更新流程定义，因为我们目前在实例中更改。如果勾选，那么修改会更改到工作流定义中去，取消的话不影响定义。
Furthermore we add a sleep code to make script timeout, and then save it. 
Here is a check whether to update the process definition, because we are currently changing it in the instance. 
If checked, the modification will be changed to the process definition. If it is canceled, the definition will not be affected.
我们选中，再去看工作流定义这边，也被更改了。
We check it and go to see the process definition side. It was also changed.
那我们再运行一下，这边需要等待70秒。（简单的demo 70s 会不会太久了  或者 这段时间可以尝试别的什么讲解或者分享一些实用小技巧）
 Then we run it again, here we need to wait 70 seconds.

（任务流程 是否添加 时间调度配置讲解  丰富任务执行流程？ 毕竟这种功能还是很常用的）

任务实例-查看日志-查看历史
目前工作流是executing状态,我们可以点击查看历史，就进入了任务实例页面。里面有我们的执行时间段，同样可以查看日志。
We can click to view the history and enter the task instance page. There are task execution time period, and we can also view the logs here.
资源中心-创建文件夹-创建文件-工作流引用资源文件
目前工作流实例为工作状态
Currently, process instance is in running status
我们先去看一下资源中心，1.3.0之后我们新增了文件夹管理的功能。我们可以创建个文件夹，再创建新的sh文件，这里支持很多格式，我们可以根据需要来创建。我们写一句echo，然后保存一下，这样文件就创建好了。
Let's take a look at the resource page first. After version 1.3.0, we have added the folder and file management. We can create a folder, and create a new sh file. DS supports many file formats here, and we can create as needed. We write an echo script, and then save it, so that the file is done.
然后我们来看资源怎么使用，如果要编辑工作流定义首先要置为下线状态。再进行编辑，资源这里，刚才创建的文件就可以选中了。
Then we look at how to use resources. If you want to edit the process definition, you must put it offline first. Edit again. Here,  we can select the file created just now as resource.
然后我们使用sh 引用资源，使用相对路径。这样就引用进来了，再保存一下。
Then we use sh to refer to run the resource and use a relative path. This is how to refer a resource file, and then save it.
-重新上线-查看日志-引用成功
我们看看刚才sleep70 秒的工作流实例。这里它已经失败了一次，失败之后它进行了一次重试。第二次重试它依然是运行的状态。
Then we take a look at the process instance that sleep 70 seconds. Here it has failed once, and after the failure it made a retry. It is still running in the retry.
我们看这条失败实例的日志，在sleep之后的代码便没有执行。没有finish说明它已经超时，并失败了。
We check the failed process instance log, script after sleep code didn't execute, There isn't finish in the prints means that the process instance timed out and failed.
工作流实例这里是可以进行重跑的，也可以停止，暂停执行中的任务。暂停后可以恢复运行，状态之间是可以转换的。
here The process instance  can be rerun, or it can be stopped, and the task in running can be suspend. The operation can be resumed after the suspension, and the state could be switched.
目前两条任务实例都失败了。
//Currently, both task instances have failed.
我们现在去运行，引用资源的工作流实例。首先我们上线它，再运行。
Then we run the process that reference a sh resource file. First we put it online, and run it.
我们等待日志，看看资源是否被引用 。这行便是资源文件中打印的内容，说明成功引用了。
We wait for the log to see if the resource is referenced. This line is printed by the resource file, indicating that it was successfully quoted.
项目管理-项目首页-任务统计
最后我们来看一下项目首页，这里统计出了单个项目的执行结果统计。
Finally, let's take a look at the project home page, where the statistics of the execution results of a single project are counted.
首页-任务统计
首页统计了所有项目的运行结果统计。
The homepage counts the running result statistics of all projects.

(讲解结束时，可以分享一个生产中使用的 shell 相关任务（脱敏） 贴上一个)

好，今天的工作流实践就到这里，有什么疑问欢迎大家添加微信群交流(贴上二维码) 感谢大家。
Well, today’s process practice ends here.Thanks all
